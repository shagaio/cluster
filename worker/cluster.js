var cluster = require('cluster');

if (cluster.isMaster) {
    cluster.setupMaster({
        exec: "worker/test.js",
        args: ["--use", "https"]
    });
    cluster.on('exit', function(worker, code, signal) {
        if (worker.suicide === true) {
            console.log('Oh, it was just suicide\' – no need to worry');
            return;
        }
        cluster.fork();
    });
    var cpus = require('os').cpus().length;
    for(var i = 0; i < cpus; i++){
        cluster.fork();
    }
}