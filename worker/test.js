var kue = require('kue');
var config = require("config");
var _ = require("underscore");
// var loggly =  require('loggly').createClient(config.loggly);
var firebase = new(require("firebase-client"))(config.firebase);
var request = require("request");

var Promise = require("promise");
function getORGANIZATION(provinceId){
    return new Promise(function (resolve){
        request({
            uri: "http://www.shilendans.gov.mn/main/find/localAjax/",
            method: "POST",
            form: {
                provinceId: provinceId
            }
        }, function(error, response, data) {
            resolve(JSON.parse(data));
        })
    })
}


function next(i) {
    getData(i).then(function (data){
        if (!data.expense){
            return next(i + 1);
        }
        // console.log(data);
        // data = _.object(data.data.main.map(_.property("ORGANIZATION_ID")), data.data.main) 
        console.log(data);
        
        data.expense = data.expense.name.map(function (name, index){
            return {
                name : name,
                dataP: data.expense.dataP[index].y,
                dataC: data.expense.dataC[index].y
            }
        })
        
        data.fund = data.fund.name.map(function (name, index){
            return {
                name : name,
                dataP: data.fund.dataP[index].y,
                dataC: data.fund.dataC[index].y
            }
        })
        
        console.log(data.fund);
        // data.map(function (){})
        // Promise.all(data.data.main.map(function (data){
        //     return firebase.set("organization/" + data.ORGANIZATION_ID, data);
        // }))
        firebase.set("organization/" + i + "/data/" + 2015, data)
        .then(function (){
            next(i + 1);
        })
        .catch(function (error){
             console.log(error);
        })
        
    })
}
function getData(agency){
    return new Promise(function (resolve){
        request({
            uri: "http://www.shilendans.gov.mn/expense_fund/expenseFundColumn",
            method: "POST",
            headers: {
                Referer: "http://www.shilendans.gov.mn/main/agency/" + agency
            },
            form: {
                id: agency,
                code: "F02",
                year: 2015,
                period: 2015,
                frequency: "Y"
            }
        }, function(error, response, data) {
            resolve(JSON.parse(data));
        })
    })
}
next(28);