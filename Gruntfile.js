module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-git-deploy');
    grunt.initConfig({
        git_deploy: {
            your_target: {
                options: {
                    url: 'git@bitbucket.org:shagaio/cluster.git',
                    branch : 'master'
                },
                src: '.'
            },
        },
    })
}