var config = require('config');
var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');
var loggly =  require('loggly').createClient(config.loggly);
var db = require("then-redis").createClient(config.redis);
app.use(express.static(path.join(__dirname, './client')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}))

app.all("/*", function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.options("*", function (req, res, next){
    res.send(200);
});
app.get("/", function (req, res, next){
    res.send(200);
});

app.post("/command/:id", function (req, res, next){
    res.send(200);
    
    db.publish("tasks:" + req.params.id, [JSON.stringify(req.body)]);
});
var kue = require('kue'); 
kue.createQueue(config.kue);
app.use(kue.app);
var http = require('http');
var server = http.createServer(app);
server.listen(process.env.PORT, process.env.IP, function() {
    console.log("Server started...");
});

var Promise = require("promise");

// var firebase = new(require("firebase-client"))(config.firebase);
var jobs = kue.createQueue(config.kue);
function find(id) {
    return new Promise(function (resolve, reject){
        kue.Job.get(id, function(err, job) {
            if (err){
                return resolve();
            }
            resolve(job);
        })
    });
}
function del(id) {
    find(id).then(function (job){
        job.remove();
    });
}
function update(data) {
    var firebase = new(require("firebase-client"))(config.firebase);
    var id = data.id
    find(id).then(function (job){
        if (!job){
            return;
        }
        return firebase.set("jobs/" + id, job)
    })
    .catch(function (error){
        console.log("err", id, error);
        db.send("LPUSH", ["firebase", JSON.stringify(data)]);
    })
    
}
setInterval(function (){
    db.send("LPOP", ["firebase"]).then(function (data){
        if (!data){
            return;
        }
        data = JSON.parse(data);
        update(data);
    });
}, 
10)