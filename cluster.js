var config = require('config');
var cluster = require('cluster');
var loggly =  require('loggly').createClient(config.loggly);
if (cluster.isMaster) {
    cluster.setupMaster({
        exec: "server.js",
        args: ["--use", "https"]
    });
    cluster.on('exit', function(worker, code, signal) {
        if (worker.suicide === true) {
            loggly.log('Oh, it was just suicide\' – no need to worry');
            return;
        }
        cluster.fork();
    });
    var cpus = require('os').cpus().length;
    for(var i = 0; i < cpus; i++){
        cluster.fork();
    }
}